import './App.css';
import { Posts} from './components/Posts'
import { QueryClient, QueryClientProvider} from 'react-query'
import { ReactQueryDevtools} from 'react-query/devtools'

const queryClient = new QueryClient()

function App() {
  return (
    <QueryClientProvider client={queryClient}>

  

        <div className="App">
       <div className="App">
          <h1>Artworks Posts with React Query</h1>
          <Posts />
          </div>
          </div>
          <ReactQueryDevtools />
      </QueryClientProvider>
  );
}

export default App;
