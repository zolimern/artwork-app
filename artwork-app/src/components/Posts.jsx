import { useState, useRef, useEffect } from "react";

import { PostDetail } from "./PostDetail";

import { useQuery, useQueryClient} from 'react-query'

const maxPostPage = 10;

async function fetchPosts(pageNum, size) {
  const response = await fetch(
    `https://api.artic.edu/api/v1/artworks?page=${pageNum}&limit=${size}`
  );

 
  return response.json();
}

export function Posts() {
  const [currentPage, setCurrentPage] = useState(0);
  const [selectedPost, setSelectedPost] = useState(null);
  const [likedPost, setLikedPost] = useState([])
  const [size, setSize] = useState(10)
  const [toggle, setToggle] = useState(false)

  const [currentData, setCurrentData ] = useState([])

  const queryClient = useQueryClient()

  const {data, isError,error, isLoading, isFetching, refetch}  = useQuery(['artworks-post', currentPage], () => fetchPosts(currentPage, size), {
    
    keepPreviousData: true
  });

  useEffect(() => {

    
    if(currentPage < maxPostPage) {
      console.log('here')

   

      const nextPage = currentPage + 1
  
      queryClient.prefetchQuery(["posts", nextPage], ()=> fetchPosts(nextPage))
    }


  }, [currentPage, queryClient, size])


  useEffect(() => {

    if(data){

      let temp = data.data.map(d => ({...d, like: false}))
      setCurrentData(temp)
      

    }

  }, [ data ])

  // using useQuery

  if(!data || isLoading || isFetching){
    return <h2>Loading...</h2>
  }
  if(isError){
   
    return <h2>{ error.message}</h2>
  }

  if(data.error){
    return <h2>{ data.error}</h2>
  }
  
  console.log(likedPost)
  
  const toggleLiked = (postId) =>{
    
    console.log(postId)
    setCurrentData(currentData.map((art) => {
     
      return art.id === postId ? { ...art, like : !art.like} : art
    }
    ))
    
    
  }
  console.log(currentData)

  const toggleLike = () => {
    setToggle(prev => !prev)
  }


  return (
    <>
    
      <ul>
        {currentData.map((post) => (
          <div style={{display: 'flex'}}>
          <li
            key={post.id}
            className="post-title"
            onClick={() => setSelectedPost(post)}
          >
            {post.title}
          </li>
          <li style={{ marginRight : 10, listStyle: 'none'}}>

            <span style={{cursor: 'pointer'}} onClick={() => {setLikedPost(prev => [...prev, post]); toggleLiked(post.id)
            currentData.map(art => {
              if(art.id === post.id){
                console.log(art.id)
               
                art.like = true
                post.like = true
                console.log(art.like)
                console.log(post.like)
              }
              return art
            });
            
            }}>{console.log(post.like)}{post.like? '💗':'🤍'}</span> 
            
          </li>
            
          </div>
        ))}
      </ul>
      <div className="pages" style={{ display: 'flex', flexDirection: 'column', columnGap: '50px', justifyContent: 'space-evenly', alignContent: 'space-between'}}>
        <div style={{ marginBottom: '50px', display: 'flex', justifyContent: 'space-between' }}>

        <button disabled={ currentPage <1 } onClick={() => {setCurrentPage(prev => prev - 1)}}>
          Previous page
        </button>
        <span>Page {currentPage + 1}</span>
        <button disabled={ currentPage >= maxPostPage} onClick={() => {setCurrentPage(prev => prev + 1)}}>
          Next page
        </button>
        </div>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>

        <select value={size} onChange={(e) => {setSize(e.target.value); refetch()}}>
            <option value={10}>size 10</option>
              <option value={20}>size 20</option>
              <option value={25}>size 30</option>
          </select>
        <button onClick={toggleLike}>{ !toggle ? 'show liked artworks' : 'hide liked arts'}</button>
        </div>
      </div>
      <hr />
      {selectedPost && !toggle && <PostDetail post={selectedPost} />}
      {toggle && likedPost.map(post => <h2>{post.title}</h2>)}
    </>
  );
}
