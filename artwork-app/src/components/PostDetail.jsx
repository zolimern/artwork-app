export function PostDetail({ post }) {

  console.log(post)
 

  if(!post){
    return <div />
  }

  return (
    <>

    <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>

      <h4>Details</h4>
      <h3 style={{ color: "blue" }}>{post.title}</h3>
      
      <p>{post.body}</p>
      <div>Artist:</div>
      <p>{ post.artist_display}</p>
      <div>Category:</div>
      <p>{post.category_titles}</p>
      <div>{ post.exhibition_history ? 'Exhibition:' : '' }</div>
      <p>{post.exhibition_history}</p>
    </div>
    
      
      
    </>
  );
}
